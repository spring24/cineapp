package com.springtraining.cineapp.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.springtraining.cineapp.models.Movie;

import com.springtraining.cineapp.services.MovieRepository;

@RestController
public class MovieResource {

	@Autowired
	MovieRepository movieService;

	@RequestMapping(value = "/movie", method = RequestMethod.GET)
	public List<Movie> getAllMovies() {
		return movieService.findAll();
	}
	
	@GetMapping("/movie/{id}")
	public Movie getMovie(@PathVariable("id") Long id) {
		return movieService.findById(id).get();
	}
	
	@PostMapping("/movie")
	public ResponseEntity<Void> createMovie(@RequestBody Movie movie) {
		//Obtener usuario de sesion y de jwt
		Movie createdMovie = movieService.save(movie);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(createdMovie.getId()).toUri();
		//Status 201
		return ResponseEntity.created(uri).build();
	}
	
	@PutMapping("/movie")
	public ResponseEntity<Movie> updateMovie(@RequestBody Movie movie) {
		//Obtener usuario de sesion y de jwt
		Movie updatedMovie = movieService.save(movie);
		return new ResponseEntity<Movie>(updatedMovie, HttpStatus.OK);
	}
	
	@DeleteMapping("/movie/{id}")
	public ResponseEntity<Void> deleteMovie(@PathVariable("id") Long id) {
		//Validar como verificar la eliminacion
		movieService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
}
