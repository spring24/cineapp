package com.springtraining.cineapp.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springtraining.cineapp.models.Rating;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {

}
