package com.springtraining.cineapp.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springtraining.cineapp.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
