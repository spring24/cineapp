package com.springtraining.cineapp.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springtraining.cineapp.models.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>  {

}
