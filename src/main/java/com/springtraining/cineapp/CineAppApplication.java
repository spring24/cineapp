package com.springtraining.cineapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CineAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CineAppApplication.class, args);
	}

}
